package se.fpcs.smsgateway;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.util.HashMap;
import java.util.Map;

import se.fpcs.smsgateway.model.ReceivedSMSMessage;

public class PhoneNumberWorker extends Worker {

    private static final String TAG = "PhoneNumberWorker";

    public PhoneNumberWorker(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
    }

    @Override
    public Result doWork() {

        TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//        if (ActivityCompat.checkSelfPermission(
//                getApplicationContext(), Manifest.permission.READ_SMS) !=
//                PackageManager.PERMISSION_GRANTED &&
//                ActivityCompat.checkSelfPermission(getApplicationContext(),
//                        Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
//                Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
//            // TODO: Consider calling
//            //    ActivityCompat#requestPermissions
//            // here to request the missing permissions, and then overriding
//            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//            //                                          int[] grantResults)
//            // to handle the case where the user grants the permission. See the documentation
//            // for ActivityCompat#requestPermissions for more details.
//            return Result.failure();
//        }
        Log.v(TAG, "foo");

        foo();

        // Indicate whether the work finished successfully with the Result
        return Result.success();
    }

    private void foo() {

        Cursor cursor = this.getApplicationContext().getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);

        if (!cursor.moveToFirst()) {
            Log.v(TAG, "SMS Inbox is empty");
            return;
        }

        while (cursor.moveToNext()) {

            //Map<String, String> attributeMap = new SMSMessage(getAttributesAsMap(cursor));

            Map<String, String> map = getAttributesAsMap(cursor);
            ReceivedSMSMessage receivedSMSMessage = ReceivedSMSMessage.builder()
                    ._id(map.get("_id"))
                    .build();

            Log.v(TAG, receivedSMSMessage.toString());

        }


    }

    private Map<String, String> getAttributesAsMap(Cursor cursor) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < cursor.getColumnCount(); i++) {
            map.put(cursor.getColumnName(i), cursor.getString(i));
        }
        return map;
    }
}
