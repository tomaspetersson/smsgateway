package se.fpcs.smsgateway;

import android.content.Context;
import android.widget.Toast;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EventHandlers {

    private Context mainActivity;

    public void register(){
        Toast.makeText(mainActivity, "Register", Toast.LENGTH_SHORT).show();
    }

}
