package se.fpcs.smsgateway.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class ReceivedSMSMessage {

    String _id;
    String date;
    String teleservice_id;
    String spam_report;
    String subject;
    String svc_cmd_content;
    String reply_path_present;
    String group_type;
    String type;
    String body;
    String thread_id;
    String protocol;
    String re_type;
    String re_original_body;
    String link_url;
    String locked;
    String msg_id;
    String app_id;
    String roam_pending;
    String from_address;
    String date_sent;
    String read;
    String bin_info;
    String sub_id;
    String pri;
    String re_content_type;
    String object_id;
    String re_content_uri;
    String delivery_date;
    String svc_cmd;
    String re_original_key;
    String reserved;
    String person;
    String using_mode;
    String error_code;
    String d_rpt_cnt;
    String favorite;
    String status;
    String hidden;
    String deletable;
    String announcements_scenario_id;
    String seen;
    String announcements_subtype;
    String re_recipient_address;
    String device_name;
    String cmc_prop;
    String callback_number;
    String sim_slot;
    String creator;
    String address;
    String sim_imsi;
    String correlation_tag;
    String re_body;
    String safe_message;
    String group_id;
    String service_center;
    String secret_mode;
    String re_file_name;
    String re_count_info;

}
